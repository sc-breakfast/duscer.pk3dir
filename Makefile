# https://gitlab.com/xonotic/netradiant-xonoticpack/blob/master/xonotic.game/default_build_menu.xml#L27
# Test: -bsp, -vis -fast, -light low
test: maps/duscer.map obj
	# prt srf bsp
	XDG_DATA_HOME=/usr/local/share q3map2 \
		-game xonotic \
		-fs_basepath /usr/share/xonotic \
		-fs_homepath ${HOME}/.xonotic \
		-fs_game data \
		-meta -keepLights \
		maps/duscer.map
	XDG_DATA_HOME=/usr/local/share q3map2 \
		-game xonotic \
		-fs_basepath /usr/share/xonotic \
		-fs_homepath ${HOME}/.xonotic \
		-fs_game data \
		-vis -fast -saveprt \
		maps/duscer.map
	XDG_DATA_HOME=/usr/local/share q3map2 \
		-game xonotic \
		-fs_basepath /usr/share/xonotic \
		-fs_homepath ${HOME}/.xonotic \
		-fs_game data \
		-light -cheapgrid -faster \
		maps/duscer.map

# prt srf bsp
bsp: $(subst .map,.bsp,$(wildcard models/*.map))
models/%.bsp: models/%.map
	XDG_DATA_HOME=/usr/local/share q3map2 \
		-game xonotic \
		-fs_basepath /usr/share/xonotic \
		-fs_homepath ${HOME}/.xonotic \
		-fs_game data \
		-meta -keepLights -patchmeta \
		$<

# obj mlt
obj: $(subst .bsp,.obj,$(wildcard models/*.bsp))
models/%.obj: models/%.map
	XDG_DATA_HOME=/usr/local/share q3map2 \
		-game xonotic \
		-fs_basepath /usr/share/xonotic \
		-fs_homepath ${HOME}/.xonotic \
		-fs_game data \
		-convert -format obj -shadersasbitmap -patchmeta \
		$<

clean:
	bash -c "rm -fv ./gfx/duscer_mini.tga"
	bash -c "rm -fv ./maps/duscer.autosave.map"
	bash -c "rm -fv ./maps/duscer.bak"
	bash -c "rm -fv ./maps/duscer.bsp"
	bash -c "rm -fv ./maps/duscer.prt"
	bash -c "rm -fv ./maps/duscer.srf"
	bash -c "rm -fv ./maps/duscer/*.tga"
	bash -c "rm -fv ./models/*.autosave.map"
	bash -c "rm -fv ./models/*.bak"
	bash -c "rm -fv ./models/*.bsp"
	bash -c "rm -fv ./models/*.mtl"
	bash -c "rm -fv ./models/*.obj"
	bash -c "rm -fv ./models/*.prt"
	bash -c "rm -fv ./models/*.srf"

.PHONY: all bsp obj clean
