textures/map_duscer/breakfast {
	polygonoffset
	{
		map textures/map_duscer/breakfast
		blendFunc blend
	}
}

textures/map_duscer/jeffs {
	polygonoffset
	{
		map textures/map_duscer/jeffs
		blendFunc blend
	}
}

textures/map_duscer/keepout {
	polygonoffset
	{
		map textures/map_duscer/keepout
		blendFunc blend
	}
}

textures/map_duscer/credits {
	polygonoffset
	{
		map textures/map_duscer/credits
		blendFunc blend
	}
}

textures/map_courtfun/screen_obey {
	polygonoffset
	{
		map textures/map_courtfun/screen_obey
		blendFunc blend
	}
}

textures/map_duscer/flag_red {
	deformVertexes wave 100 sin 0 5 0 2
	{
		map textures/map_duscer/flag_red
		blendFunc blend
	}
}

textures/map_warfare/cactus {
	deformVertexes autosprite2
	{
		map textures/map_warfare/cactus
		blendFunc blend
	}
}

textures/map_warfare/cactus {
	deformVertexes autosprite2
	{
		map textures/map_warfare/cactus
		blendFunc blend
	}
}
